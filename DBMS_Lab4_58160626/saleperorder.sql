CREATE VIEW SalePerOrder AS
	SELECT 
		orderNumber, SUM(quantityordered * priceEach) total
	FROM
		orderdetails
	GROUP BY orderNumber
	ORDER BY total DESC;
